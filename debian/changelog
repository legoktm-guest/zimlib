zimlib (4.0.0-1) experimental; urgency=medium

  * New upstream version 4.0.0 (Closes: #884440)
  * Build-Depend upon libgtest-dev
  * Update binary package for new soname
  * Set SKIP_BIG_MEMORY_TEST=1 during build
  * Update d/copyright for new upstream release
  * Remove unnecessary lintian override
  * Add myself (Kunal Mehta) to uploaders
  * Update Vcs-* URLs to salsa.debian.org
  * Standards-Version: 4.1.5

 -- Kunal Mehta <legoktm@member.fsf.org>  Sun, 08 Jul 2018 15:49:51 -0700

zimlib (2.0.0-2) unstable; urgency=medium

  * Modernize Vcs-* fields: Use git (not cgit) in path.
  * Declare compliance with Debian Policy 4.0.1.
  * Update symbols file.
    Closes: Bug#872464. Thanks to James Clarke.
  * Update git-buildpackage config: Filter any .gitignore file.
  * Drop obsolete README.source: CDBS no longer used.
  * Update copyright info:
    + Use only Github issue tracker as preferred form of contact.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 18 Aug 2017 17:17:32 +0200

zimlib (2.0.0-1) unstable; urgency=medium

  * New upstream release.
    Closes: bug#868641. Thanks to Kunal Mehta.
    + Move to meson build system
    + Move to C++11 standard
    + Full text search using xapian API's
    + Drop old unsupported API's
    + Remove bzip2 and symbian support.
  * debian/watch:
    + Bump watch file version to 4.0.
    + Change upstream URL to Github.
  * debian/control:
    + Mark package as compliant with Debian policy 4.0.0.
    + Drop libzim0v5 libzim0v5-dbg packages as upstream bumped soname to 2.
    + Introduce libzim2 package.
    + Drop build dependency on cdbs, dh-buildinfo and dh-buildinfo.
    + Drop build dependency on autotools-dev, automake, autoconf and
      libtool as package upstream now uses meson build system.
    + Add build dependency on meson and ninja-build
    + Add versioned build dependency on debhelper >= 9.
    + Add build dependency on libxapian-dev, zlib1g-dev and pkg-config.
  * Drop libzim0v5.symbols file.
  * debian/patches:
    + Drop patch 1001_fix_uuid_test_on_hurd.patch its merged upstream.
    + Drop upstream cherry-picked patch 0002 as its included in new release.
  * Create new libzim2.symbols file.
  * debian/copyright:
    + Move around files which are now in new paths.
    + Remove all unused references to older release (autotools files)
    + Use secure URL for Format field.
    + Add new Github URL for Upstream-Contact and Source fields.
  * Update debian/copyright_hints file for new release.
  * Add debian/copyright-check for updating copyright_hints file.
  * Add libzim2.install file for installing share library.
  * Add libzim-dev.install for installing development header and
    pkg-config files.
  * debian/source/lintian-overrides:
    + Drop unused lintian-override on versioned debhelper dependency.
    + Add lintian-override for FIXME errors in copyright_hints.
  * Drop hard coded Pre-Depends on multiarch-support package.
    Closes: bug#870517. Thanks to  Aurelien Jarno.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sat, 05 Aug 2017 16:14:26 +0530

zimlib (1.4-2.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Clean up symbols file, thanks James Clarke. (Closes: #860625)

 -- Graham Inggs <ginggs@debian.org>  Fri, 21 Apr 2017 10:06:38 +0200

zimlib (1.4-2) unstable; urgency=medium

  * Update symbols file for all architectures supported by Debian except
    alpha and sh4.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sat, 12 Nov 2016 22:57:43 +0530

zimlib (1.4-1) unstable; urgency=medium

  * New upstream release.
  * Added 0002 patch from upstream source fixing build failure.
  * Update symbols file for 1.4 release.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sat, 12 Nov 2016 18:20:26 +0530

zimlib (1.3-1) unstable; urgency=medium

  * New upstream release 1.3
  * Update libzim0v5.symbols file for new release.
  * Update copyright and copyright_hints file.
  * Replace devscripts build dependency with licensecheck

 -- Vasudev Kamath <vasudev@copyninja.info>  Tue, 11 Oct 2016 20:27:36 +0530

zimlib (1.2-12) unstable; urgency=medium

  * Update symbols file to include amd64 symbols.
    Closes: bug#831209, Thanks to Lucas Nussbaum.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sat, 16 Jul 2016 17:07:20 +0530

zimlib (1.2-11) unstable; urgency=low

  * Update the symbols file for gcc-6 release.
    Closes: bug#811976, Thanks to Martin Michlmayr
  * debian/control:
    + Mark package as compliant with Debian policy 3.9.8.
  * debian/rules:
    + Make sure empty ChangeLog file from upstream is not installed.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sun, 03 Jul 2016 13:49:34 +0530

zimlib (1.2-10) unstable; urgency=medium

  [ Jonas Smedegaard ]
  * Update watch file: Fix avoid uupdate (doesn't play well with
    git-buildpackage).
  * Fix relax symbols check when targeting experimental.
  * Update symbols file for armel armhf hurd-i386 kfreebsd-i386 m68k
    mips mipsel powerpc powerpcspe sparc64.

  [ Vasudev Kamath ]
  * Change my email address in Uploaders field.

 -- Vasudev Kamath <vasudev@copyninja.info>  Sun, 03 Jul 2016 13:32:33 +0530

zimlib (1.2-9) unstable; urgency=medium

  * Modernize Vcs-* fields:
    + Fix include /git/ part for Vcs-Git URL.
    + Use https protocol.
  * Fix put aside more upstream-shipped autotools files during build,
    and (re)create autotools.
    Closes: Bug#818456. Thanks to Martin Michlmayr.
  * Declare compliance with Debian Policy 3.9.7.
  * Modernize watch file, and add usage hint comment.
  * Drop CDBS get-orig-source target: Use "gbp import-orig --uscan"
    instead.
  * Relax symbols check when targeting experimental.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 25 Mar 2016 11:57:18 +0100

zimlib (1.2-8) unstable; urgency=medium

  * Update symbols file for amd64,arm64,kfreebsd-amd64 and mipsel64
    architectures.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Wed, 07 Oct 2015 20:49:19 +0530

zimlib (1.2-7) unstable; urgency=medium

  * Update symbols file for sh4 architecture.
    Closes: bug#800912, Thanks to John Paul Adrian Glaubitz.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Tue, 06 Oct 2015 22:22:35 +0530

zimlib (1.2-6) unstable; urgency=medium

  * Update symbols file from buildd logs.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 03 Oct 2015 11:32:14 +0200

zimlib (1.2-5) unstable; urgency=medium

  [ Vasudev Kamath ]
  * Fix FTBFS with GCC5, introduces library transition.
    Closes: bug#790296, Thanks to  Daniel Schepler.
  * Declare compliance with Debian Policy 3.9.6.
  * Use cgit front-end for Vcs-Browser.
  * Update symbols file for amd64 architecture.

  [ Jonas Smedegaard ]
  * Update copyright info:
    + Extend coverage of autotools files.
    + Rewrap at 72 chars.
    + Fix use SPDX shortname X11 (not Expat~X with X Exception).
    + Fix licensing of some autotools files.
    + Fix list licenses with exceptions as separate License sections.
    + Use License-Grant, License-Reference and License-Exception fields.
      Thanks to Ben Finney.
    + Merge Files section with same Copyright and License-Grant.
  * Override lintian regarding license in License-Reference field.
    See bug#786450.
  * Build-depend unversioned on debhelper: Needed version satisfied even
    in oldstable.
  * Override lintian regarding build-depending unversioned on debhelper.

 -- Jonas Smedegaard <dr@jones.dk>  Fri, 02 Oct 2015 10:41:53 +0200

zimlib (1.2-4) unstable; urgency=medium

  * Update symbols file for arm64.
    Closes: bug#761138, Thanks to Mathias Klose.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 13 Sep 2014 23:16:39 +0530

zimlib (1.2-3) unstable; urgency=medium

  * Update symbols file, was erroring out on kfreebsd-i386 and mips
    platform.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sun, 22 Jun 2014 22:33:45 +0530

zimlib (1.2-2) unstable; urgency=medium

  * Updated symbols file for all the architecture
  * Comment out DPKG_GENSYMBOLS_CHECK_LEVEL=0, we have fixed symbols file
    for all architecture.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sun, 15 Jun 2014 01:31:54 +0530

zimlib (1.2-1) unstable; urgency=medium

  * Import new upstream release.
  * Use , for DEB_AUTO_UPDATE_AUTOMAKE, and DEB_AUTO_UPDATE_ACLOCAL, thus
    depending on unversioned latest automake. CDBS build-depends hence
    gets bumped to 0.4.123~ (version where this feature was introduced).
  * Update symbols file for new release, with few new symbols being introduced
  * Update upstream tarball md5 hash
  * Set DPKG_GENSYMBOLS_CHECK_LEVEL to 0 to prevent build failure other
    architecture where symbol file is not updated.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Wed, 11 Jun 2014 22:17:19 +0530

zimlib (1.1-4) unstable; urgency=medium

  * Update symbols file for all officially supported architecture.
  * Comment out DPKG_GEN_SYMBOLS_CHECK=0 which prevents dpkg-gensymbols
    from failing when new symbols were added.
  * Bump standards-version to 3.9.5

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Tue, 21 Jan 2014 22:51:54 +0530

zimlib (1.1-3) unstable; urgency=medium

  * update copyright_hints.
  * Use version 1.14 for DEB_AUTO_UPDATE_ACLOCAL and DEB_AUTO_UPDATE_AUTOMAKE
    Closes: bug#730042, Thanks to Daniel Schelper.
  * manully modify control to use automake (>= 1.14) for build-depends.
  * Update symbols file, 2 symbols updated for amd64 architecture.

 -- Vasudev Kamath <kamathvasudev@gmail.com>  Sat, 18 Jan 2014 12:12:27 +0530

zimlib (1.1-2) unstable; urgency=low

  [ Vasudev Kamath ]
  * Update symbols files for all Linux architectures and kFreeBSD. (i.e.
    Hurd excluded).
  * Add patch 1001 putting delay betweeen UUID tests in testsuite,
    fixing FTBFS on Hurd.
    Thanks to Pino Toscano.
  * Bumped automake dependency to 1.13.

  [ Jonas Smedegaard ]
  * Add tarball checksum.
  * Add debian/patches/README documenting numbering micro policy.
  * Refresh patch with shortening quilt options.

 -- Jonas Smedegaard <dr@jones.dk>  Sat, 14 Sep 2013 17:23:08 +0200

zimlib (1.1-1) unstable; urgency=low

  * Initial release.
    Closes: bug#698111.

 -- Jonas Smedegaard <dr@jones.dk>  Mon, 09 Sep 2013 19:32:21 +0200
